import './App.css';
import { Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import Navbar from './components/Navbar';
import Categories from './components/Categories';
import CardComponent from './components/CardComponent';
import NewArticle from './components/NewArticle';

function App() {
  return (
    <div >
      <Navbar/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/categories' element={<Categories/>}/>
        <Route path='/categories/edit' element={<CardComponent/>}/>
        <Route path='/newarticle' element={<NewArticle/>}/>
      </Routes>
    </div>
  );
}

export default App;

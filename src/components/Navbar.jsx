import React from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function NavComponent() {
    return (
        <div id='font'>
            <Navbar bg="light" variant="light">
                <Container>
                    <Navbar.Brand href="">React-Bootstrap</Navbar.Brand>
                    <Nav className="ms-auto">
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to="/categories">Categories</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </div>
    );
}

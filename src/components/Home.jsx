import React from 'react'
import { Button, Container, Nav, Navbar } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Home() {
  return (
    <div id='font'>
      <Navbar>
        <Container>
          <Navbar.Brand href="">All Article</Navbar.Brand>
          <Nav className="ms-auto">
            <Button className="btn btn-primary" as={Link} to="/newarticle">New Article</Button>
          </Nav>
        </Container>
      </Navbar>
    </div>
  )
}

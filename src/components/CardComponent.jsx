import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CardComponent() {
  return (
    <div>
        <Button as = {Link} to = "/categories/edit">Edit</Button>
    </div>
  )
}

import React from 'react'
import { Card, CardGroup, Navbar } from 'react-bootstrap'

export default function Categories() {
  return (
    <div id='font'>
      <Navbar.Brand href="">All Article</Navbar.Brand>
      <CardGroup>
        <Card>
          <Card.Img variant="top" src="https://autosdutriomphe.fr/wp-content/uploads/2018/04/default-image.png" width={280} />
          <Card.Body>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 5sec ago</small>
          </Card.Footer>
        </Card>
        <Card>
          <Card.Img variant="top" src="https://autosdutriomphe.fr/wp-content/uploads/2018/04/default-image.png" />
          <Card.Body>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 5sec ago</small>
          </Card.Footer>
        </Card>
        <Card>
          <Card.Img variant="top" src="https://autosdutriomphe.fr/wp-content/uploads/2018/04/default-image.png" />
          <Card.Body>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">Last updated 5sec ago</small>
          </Card.Footer>
        </Card>
      </CardGroup>
    </div>
  )
}

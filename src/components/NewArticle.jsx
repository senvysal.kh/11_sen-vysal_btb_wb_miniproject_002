import { Button, Card, Container, Form } from 'react-bootstrap'

export default function NewArticle() {
    return (
        <div>
            <Container id='font1'>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" placeholder="Enter title" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Enter description" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="published" />
                    </Form.Group>
                    <div>
                        <img src="https://autosdutriomphe.fr/wp-content/uploads/2018/04/default-image.png" alt="" width={280} />
                        <Card.Title>image file</Card.Title>
                        <Card>
                            <Card.Img variant="top" />
                            <Card.Body id='body'>
                                <input type="file" />
                            </Card.Body>
                        </Card>
                    </div>
                </Form>
                <br />
                <div className='d-grid gap-2 d-md-flex justify-content-md-end'>
                    <Button variant="danger" type="calcle">Cancle</Button>
                    <Button variant="primary" type="submit">Submit</Button>
                </div>
            </Container>
        </div>
    )
}
